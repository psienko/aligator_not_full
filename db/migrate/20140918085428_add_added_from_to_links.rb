class AddAddedFromToLinks < ActiveRecord::Migration
  def change
    add_column :links, :added_from, :string
  end
end
