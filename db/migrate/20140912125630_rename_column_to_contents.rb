class RenameColumnToContents < ActiveRecord::Migration
  def change
    rename_column :contents, :github_readme, :content
  end
end
