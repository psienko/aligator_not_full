class AddLinkIdToContents < ActiveRecord::Migration
  def change
    add_column :contents, :link_id, :integer
  end
end
