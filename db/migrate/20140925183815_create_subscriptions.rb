class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :user
      t.integer :subscribeable_id
      t.string :subscribeable_type

      t.timestamps
    end
  end
end
