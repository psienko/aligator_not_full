class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :content
      t.integer :link_id
      t.integer :user_id

      t.timestamps
    end
    add_index :comments, [:link_id, :created_at, :user_id]
  end
end
