class FixColumnNameTittle < ActiveRecord::Migration
  def change
    change_table :links do |t|
      t.rename :text, :title
    end
  end
end
