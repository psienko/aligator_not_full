class AddScreenshotUpdateFieldToLinks < ActiveRecord::Migration
  def change
    add_column :links, :screenshot_update_time, :datetime
  end
end
