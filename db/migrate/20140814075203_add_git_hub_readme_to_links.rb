class AddGitHubReadmeToLinks < ActiveRecord::Migration
  def change
    add_column :links, :gh_readme, :text
  end
end
