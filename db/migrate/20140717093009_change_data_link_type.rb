class ChangeDataLinkType < ActiveRecord::Migration
  def change
    change_column :links, :link_data, :text
  end
end
