class AddContentUpdateTimeToContents < ActiveRecord::Migration
  def change
    add_column :contents, :content_update_time, :datetime
  end
end
