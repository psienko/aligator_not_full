class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.text :github_readme

      t.timestamps
    end
  end
end
