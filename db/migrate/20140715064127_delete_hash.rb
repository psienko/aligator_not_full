class DeleteHash < ActiveRecord::Migration
  def change
    ActsAsTaggableOn::Tag.all.each do |t|
      t.name=/[^#]+/.match(t.name).to_s
      t.save!
    end
  end
end
