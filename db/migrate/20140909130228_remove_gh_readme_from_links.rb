class RemoveGhReadmeFromLinks < ActiveRecord::Migration
  def change
    remove_column :links, :gh_readme
  end
end
