require 'spec_helper'

describe GithubReadmeMaker do
  describe 'fetches and writes github readme' do
    let(:link) { FactoryGirl.create(:link, url: 'http://www.github.com/ramvs/ruby', title: 'videochat') }
    it 'featch and write correct github readme' do
      VCR.use_cassette('github_response_with_path') do
        GithubReadmeMaker.call(link)
        expect(link.github_exist?).to be true
        expect(link.content_exist?).to be true
      end
    end
  end
end
