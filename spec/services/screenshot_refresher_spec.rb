require 'spec_helper'

def screenshot_file_time_modified(link)
  file = File.join(Rails.root, 'tmp', 'faketests', 'screenshots', "link_#{link.id}.png")
  File.mtime(file)
end

describe "Screenshot refresher", js: true do
  before(:all) do
      FileUtils.rm_rf(Dir.glob("tmp/faketests/screenshots/*"))
  end

  context "Link without Last-Modified" do
    let(:link){ FactoryGirl.create :link, url: "http://www.onet.pl",
                                   screenshot_update_time: Time.now - 2.day }

    it "should have actual date screenshot update time" do
      VCR.use_cassette('link_without_last_modified') do
        old_screenshot_update_time = link.screenshot_update_time
        ScreenshotRefresher.call link
        expect(link.screenshot_update_time).to be > old_screenshot_update_time
      end
    end

    it "should have actual screenshot image " do
      VCR.use_cassette('link_without_last_modified') do
        old_screenshot_update_time = link.screenshot_update_time
        ScreenshotRefresher.call link
        expect(screenshot_file_time_modified(link)).to be > old_screenshot_update_time
      end
    end

  end

  context "Link with actual Last-Modified" do
    let(:link) { FactoryGirl.create :link, url: "http://www.interia.pl",
                                    screenshot_update_time: Time.now + 1.day }

    it "should won't update screenshot update date" do
      VCR.use_cassette('link_with_actual_last_modified') do
        old_screenshot_update_time = link.screenshot_update_time
        ScreenshotRefresher.call link
        expect(link.screenshot_update_time).to eq(old_screenshot_update_time)
      end
    end

  end

  context "Link with out-of-date Last-Modified" do
    let(:link)  { FactoryGirl.create :link, url: "http://www.wp.pl",
                                     screenshot_update_time: "2001-01-01" }

    it "should have actual screenshot update date" do
      VCR.use_cassette('link_with_outdate_last_modified') do
        old_screenshot_update_time = link.screenshot_update_time
        ScreenshotRefresher.call link
        expect(link.screenshot_update_time).to be > old_screenshot_update_time
      end
    end

    it "should have actual screenshot image" do
      VCR.use_cassette('link_with_outdate_last_modified') do
        old_screenshot_update_time = link.screenshot_update_time
        ScreenshotRefresher.call link
        expect(screenshot_file_time_modified(link)).to be > old_screenshot_update_time
      end
    end
  end
end
