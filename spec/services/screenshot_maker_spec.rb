require 'spec_helper'

describe ScreenshotMaker, js: true do
  describe "making screenshot" do

    let(:valid_link) { FactoryGirl.create(:link,
                                          url: 'http://www.onet.pl', title: 'onet') }
    let(:ftp_link) { FactoryGirl.create(:link, title: 'ftp with ftp scheme',
                                        url: 'ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt') }
    let(:ftp_link_with_http) { FactoryGirl.create(:link, title: 'ftp with http scheme',
                                                  url: 'http://ftp.funet.fi/pub/standards/RFC/rfc959.txt') }

    before(:all) do
      FileUtils.rm_rf(Dir.glob("tmp/faketests/screenshots/*"))
    end

      it 'create screenshot for valid url' do
        ScreenshotMaker.call(valid_link)
        expect(valid_link.screenshot_exist?).to be true
      end

    it "doesn't create screenshot for ftp scheme links" do
      ScreenshotMaker.call(ftp_link)
      expect(ftp_link.screenshot_exist?).to be false
    end

    it "doesn't not create screenshot for ftp links" do
      ScreenshotMaker.call(ftp_link_with_http)
      expect(ftp_link_with_http.screenshot_exist?).to be false
    end
  end
end
