require 'spec_helper'

describe "Featching link content - refresher" do
  context "Link without Last-Modified" do
    let(:link) { FactoryGirl.create :link, url: "http://www.onet.pl" }

    it "should have updated content_update_time" do
      VCR.use_cassette('content_link_without_last_modified') do
        old_content_update_time = link.content.content_update_time
        LinkContentRefresher.call link
        expect(link.content.content_update_time).to be > old_content_update_time
      end
    end
  end

  context "Link with actual Last-Modified" do
    let(:link) { FactoryGirl.create :link, url: "http://www.interia.pl" }

    it "should not update content update date" do
      VCR.use_cassette('content_link_with_actual_last_modified') do
        link.content.content_update_time = (Time.now + 2.days).utc
        old_content_update_time = link.content.content_update_time
        LinkContentRefresher.call link
        expect(link.content.content_update_time).to eq(old_content_update_time)
      end
    end

  end

  context "Link with out-of-date Last-Modified" do
    let(:link)  { FactoryGirl.create :link, url: "http://www.wp.pl" }

    it "should have updated content update date" do
      VCR.use_cassette('content_link_with_outdate_last_modified') do
        old_content_update_time = link.content.content_update_time
        LinkContentRefresher.call link
        expect(link.content.content_update_time).to be > old_content_update_time
      end
    end
  end

end
