require 'spec_helper'

describe FetchContentPageMaker do
  describe 'fetches and save content of webpage' do
    let(:page_link) { FactoryGirl.create(:link, url: 'http://www.onet.pl') }
    it 'fetch and save correct content of page' do
      VCR.use_cassette 'content_page_response' do
        FetchContentPageMaker.call(page_link)
        expect(page_link.content_exist?).to be true
      end
    end
  end
end
