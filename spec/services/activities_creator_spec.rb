require 'spec_helper'

describe ActivitiesCreator do
  describe 'create activity for link' do
    let(:link) { FactoryGirl.create(:link) }
    it 'take link param and adds to it activity' do
      expect(ActivitiesCreator.create_link_activity(link)).to eq link.activities.last
    end
  end
end
