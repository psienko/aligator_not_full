require 'spec_helper'

RSpec.describe "links/index", :type => :view do
  before(:each) do
    assign(:links, [
      Link.create!(
      url: "http://www.onet.pl",
      tag_list: "#hash",
      title: "MyTitle"
      ),
      Link.create!(
      url: "http://www.onet.pl",
      tag_list: "#hash",
      title: "MyTitle"
      )
    ])
  end
end
