VCR.configure do |c|
  c.ignore_localhost = true
  c.cassette_library_dir = 'spec/cassette'
  c.hook_into :webmock
end
