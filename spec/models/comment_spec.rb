require 'spec_helper'

describe "Comment" do

  let(:link) { FactoryGirl.create :link, url: "google.com",
               title: "google"}
  let(:user) { FactoryGirl.create(:user, email: "foobar@gmail.com",
                                  password: "samplepass")}
  let(:comment) { FactoryGirl.create(:comment, content: "Sample Content",
                                     link_id: link.id, user_id: user.id)}
  let(:older_comment) { FactoryGirl.create(:comment, link_id: link.id,
                                           user_id: user.id, created_at: 1.day.ago)}
  let(:newer_comment) { FactoryGirl.create(:comment, link_id: link.id,
                                           user_id: user.id, created_at: 1.hour.ago)}

  it "requires a link id" do
    comment.link_id = nil
    expect(comment).to have(1).error_on(:link_id)
  end

  it "by passing link_id" do
    expect(comment.owner_email).to eq("foobar@gmail.com")
  end
end
