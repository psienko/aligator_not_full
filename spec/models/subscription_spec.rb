require 'spec_helper'

describe Subscription do
  let(:user) { FactoryGirl.create :user, email: "example@example.com" }
  let(:user_subscription) { FactoryGirl.create :user_subscription }

  describe "creation new subscription" do
    context "User has not subscribed before" do
      it "assigns user to subscryption" do
        user_subscription.assign_user_to_subscription(user.id)
        expect(user_subscription.user_id).to eq(user.id)
      end
    end

    context "User has subscribed before" do
      it "does not assign to subscryption" do
        subscription = FactoryGirl.create :user_subscription, user_id: user.id
        expect(subscription.assign_user_to_subscription(user.id)).to be false
      end
    end

    context "User want to subscribe oneself" do
      it "does not assigned to subscryption" do
        user_subscription.subscribeable = user
        expect(user_subscription.assign_user_to_subscription(user.id)).to be false
      end
    end
  end
end
