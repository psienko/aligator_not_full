require 'spec_helper'

describe Link do
  let(:link) do
    FactoryGirl.create :link, url: "google.com", title: "google"
  end

  after(:each) do
    Link.index.delete
    hash_data = Hash.new
  end

  describe "Checking data parsing" do
    context "allowed to use links" do
      it "with numbers" do
        link.parse_and_save("http://www.rails4zombies.org title")
        expect(link.url).to eq("http://www.rails4zombies.org")
      end

      it "from video portal" do
        link.parse_and_save("https://www.youtube.com/watch?v=BxcDSSbVUk0 title")
        expect(link.url).to eq("https://www.youtube.com/watch?v=BxcDSSbVUk0")
      end

      it "complex url address" do
        link.parse_and_save("http://www.youtube.com/watch?v=8SRqRfAq78M title")
        expect(link.url).to eq("http://www.youtube.com/watch?v=8SRqRfAq78M")
      end
      it "ftp" do
        link.parse_and_save("ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt title")
        expect(link.url).to eq("ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt")
      end
      
      it 'with www in front' do
        link.parse_and_save("www.onet.pl title #tag")
        expect(link.url).to eq 'http://www.onet.pl'
      end

      it 'simple url address' do
        link.parse_and_save("onet.pl title #tag")
        expect(link.url).to eq 'http://onet.pl'
      end
    end

    context 'diffrent places url in string' do
      it 'in front of string' do
        link.parse_and_save('wp.pl title #wp')
        expect(link.url).to eq 'http://wp.pl'
      end

      it 'in the midlle of string' do
        link.parse_and_save('#tag title_p1 onet.pl title_p2 #tag2')
        expect(link.url).to eq 'http://onet.pl'
      end

      it 'in the end of string' do
        link.parse_and_save('#tag1 title #tag2 onet.pl')
        expect(link.url).to eq 'http://onet.pl'
      end
    end

    context "type of links with errors" do

      it "with empty http" do
        link.parse_and_save("http:// title")
        expect(link).to have(1).error_on(:url)
      end

      it "without dot" do
        link.parse_and_save("fakelink title")
        expect(link).to have(1).error_on(:url)
      end

      it "with wrong host" do
        link.parse_and_save("http://.a title")
        expect(link).to have(1).error_on(:url)
      end

      it 'with space after http://' do
        link.parse_and_save("http:// onet.pl title")
        expect(link).to have(1).error_on(:url)
      end
      it 'with space after www.' do
        link.parse_and_save("http://www. onet.pl title")
        expect(link).to have(1).error_on(:url)
      end
      it 'with space in domain' do
        link.parse_and_save("www.test onet.pl title")
        expect(link).to have(1).error_on(:url)
      end
    end

    context "persistance" do
      it "save" do
        link.parse_and_save("https://google.pl title")
        expect(link).to be_persisted
      end
    end
  end

  describe "Adding link_data add tags, url, title" do
    before :each do
      link.parse_and_save("#first http://www.onet.pl #third News can be find here #second")
    end

    it "update url" do
      expect(link.url).to eq "http://www.onet.pl"
    end

    it "update tags" do
      expect(link.tag_list).to eq ["first", "third", "second"]
    end

    it "update title" do
      expect(link.title).to eq "News can be find here"
    end

    it "save" do
      expect(link).to be_persisted
    end
  end

  describe "scheme check" do

    it "http link returns http link" do
      link.url = "http://www.onet.pl"
      expect(link.url_with_scheme).to eq "http://www.onet.pl"
    end

    it "no scheme links returns http link" do
      link.url = "www.onet.pl"
      expect(link.url_with_scheme).to eq "http://www.onet.pl"
    end

    it "ftp scheme link returns ftp link" do
      link.url = "ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt"
      expect(link.url_with_scheme).to eq "ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt"
    end

    it "add http scheme to ftp links" do
      link.url = "ftp.funet.fi/pub/standards/RFC/rfc959.txt"
      expect(link.url_with_scheme).to eq "http://ftp.funet.fi/pub/standards/RFC/rfc959.txt"
    end
  end

  describe "http check" do
    it "http link returns true" do
      link.url = "http://www.onet.pl"
      expect(link.http?).to be true
    end

    it "no scheme link returns false" do
      link.url = "www.onet.pl"
      expect(link.http?).to be false
    end

    it "ftp scheme link returns false" do
      link.url = "ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt"
      expect(link.http?).to be false
    end
  end

  describe 'title chcek' do
    context 'title without quotes' do
      it 'title added to title field' do
        link.parse_and_save("http://www.wp.pl news and informations #wp #news")
        expect(link.title).to eq "news and informations"
      end
    end
    context 'title with quotes' do
      it 'title added to title field' do
        link.parse_and_save("http://www.wp.pl 'news and informations' #wp #news")
        expect(link.title).to eq "'news and informations'"
      end
    end
  end

  describe "link owner" do
    let(:user) { FactoryGirl.build :user, email: "user@example.com" }
    let(:link_with_user) { FactoryGirl.create :link, user: user }
    let(:link_without_user) { FactoryGirl.create :link, user: nil }

    it "link added to user" do
      expect(link_with_user.added_by).to eq("user@example.com")
    end

    it "link added to guest" do
      expect(link_without_user.added_by).to eq("Guest")
    end
  end

  describe "search link" do

    let!(:link_without_user) do
      FactoryGirl.create(:link, title: "$$$$11SECOND11", url: "http://google.pl",
                         tag_list: ["TAGG"])
    end
    let!(:link_without_user2) do
      FactoryGirl.create(:link, title: "$$$$11SECOND12", url: "http://google.pl",
                         tag_list: ["TAGG2"])
    end
    let!(:link_without_user3) do
      FactoryGirl.create(:link, title: "$$$$11SECOND12", url: "http://google.pl",
                         tag_list: ["TAGG2"])
    end

    before :each do
      # Elasticsearch
      Link.index_name(
        Link.model_name.plural + '_test_' + DateTime.now.strftime('%Y_%m_%d_%H_%M_%S')
      )
      Link.create_elasticsearch_index
      Link.import
      Link.all.each { |link| link.update_index }
      Link.index.refresh
    end

    after :all do
      Link.index.delete
    end

    context "Finding by title" do
      it "returns properly result with link id" do
        results = Link.search("$$$$11SECOND11")
        expect(results[0].id).to eq(link_without_user.id.to_s)
      end

      it "returns empty collection if not found any results" do
        expect(Link.search("*RANDWORDSRANDWORDS")).to be_empty
      end
    end

    context "Finding by tags" do
      it "return properly result with link id" do
        results = Link.search("TAGG")
        expect(results[0].id).to eq(link_without_user.id.to_s)
      end
    end
  end
end
