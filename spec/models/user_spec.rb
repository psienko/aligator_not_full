require 'spec_helper'

describe User do
  let(:user) { FactoryGirl.build :user }
  let(:user_with_same_mail) { FactoryGirl.build :user }
  it "New user change Users value" do
    user.email = "sample_email@gmail.com"
    user.password = "secret123"
    expect { user.save}.to change(User, :count).by(+1)
    user_with_same_mail.email = "sample_email@gmail.com"
    user_with_same_mail.password = "sample123"
    user_with_same_mail.save
    expect(user_with_same_mail).to have(1).error_on(:email)
  end
end
