require 'spec_helper'

describe TagsController do
  describe 'DELETE destroy' do
    context 'When tag is present in tags list' do
      it 'removes tag from tags list' do
        link = FactoryGirl.create :link,
                                  url: 'www.wp.pl',
                                  tag_list: 'wirtualna polska wp',
                                  title: 'Wirtualna'
        expect { delete :destroy, link_id: link, name: 'wp' }
          .to change { link.reload.tag_list.length }.by(-1)
      end
    end

    context 'When tag is not present in tag_list' do
      it 'does not remove tag form tags list' do
        link = FactoryGirl.create :link,
                                  url: 'www.wp.pl',
                                  tag_list: 'wirtualna polska wp',
                                  title: 'Wirtualna'
        expect { delete :destroy, link_id: link, name: 'newsy' }
          .to change { link.reload.tag_list.length }.by(0)
      end
    end
  end
end
