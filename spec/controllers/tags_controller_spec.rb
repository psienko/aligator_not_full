require 'spec_helper'

describe TagsController, type: :controller do

  describe "GET 'new'" do
    it 'assigns @link and @tag' do
      link = FactoryGirl.create :link
      tag = ActsAsTaggableOn::Tag.new
      get :new, link_id: link.id
      expect(assigns(:link)).to eq(link)
      expect(assigns(:tag)). to eq(tag)
    end

    it 'renders the new template' do
      link = FactoryGirl.create :link
      get :new, link_id: link.id
      expect(response).to render_template :new
    end
  end

  describe "POST 'create'" do
    context 'with valid atributes' do
      it 'adds the new tag to existing link' do
        link = FactoryGirl.create :link
        expect do
          post :create, acts_as_taggable_on_tag: { name: 'test_ag' }, link_id: link.id
        end.to change(ActsAsTaggableOn::Tag, :count).by(1)
      end
    end

    context 'with invalid atributes' do
      it 'does not add the new tag to existing link' do
        link = FactoryGirl.create :link
        expect do
          post :create, acts_as_taggable_on_tag: { name: '' }, link_id: link.id
        end.to_not change(ActsAsTaggableOn::Tag, :count)
      end
    end

    context 'after correct or incorrect completion of the adding of the tag' do
      it 're-renders the new method' do
        link = FactoryGirl.create :link
        post :create, acts_as_taggable_on_tag: { name: 'test_tag' }, link_id: link.id
        expect(response).to redirect_to new_link_tag_path(link.id)
      end
    end
  end

end
