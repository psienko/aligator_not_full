require 'spec_helper'
describe SearchController do
  describe "GET search" do
    it "returns links collection for valid input" do
      get :search, q: "test"
      expect(assigns(:links)).to be_a Tire::Results::Collection
    end

    it "sets alert if input is invalid" do
      get :search, q: "te"
      expect(flash[:alert]).to eq("Too short query")
    end
  end

  describe "GET suggestTags" do
    it "should be successful" do
      expect(response).to be_success
    end

    it "returns array of suggested tags for query" do
      FactoryGirl.create :link, tag_list: "suggested1 suggested2"
      get :suggest_tags, query: "suggest"
      suggests = JSON.parse(response.body)
      expect(suggests).to eq(["suggested1", "suggested2"])
    end
  end
end
