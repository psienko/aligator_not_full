require 'spec_helper'

describe LinksController do
  describe '#index' do
    before do
      get :index, :format => "rss"
    end
    it "has succesfull RSS response" do
      expect(response).to be_success
    end
    it "has content type rss+xml" do
       expect(response.content_type).to have_content("application/rss+xml")
    end
  end

  describe '#create' do
    it 'create link and add content for the link' do
      link = FactoryGirl.create :link
      expect do
        post :create, link: { link_data: link.link_data }
      end.to(change(Link, :count).by(1), change(Content, :count).by(1))
    end
  end
end
