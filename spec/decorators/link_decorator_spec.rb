require 'spec_helper'

describe LinkDecorator do
  describe "url show helper" do
    let(:link_with_protocol) { FactoryGirl.build(:link, url: "http://onet.pl").decorate }
    let(:link_without_protocol) { FactoryGirl.build(:link, url: "www.onet.pl").decorate }

    context 'host' do
      it 'returns host without www' do
        expect(link_with_protocol.host).to eq("onet.pl")
      end
      it 'returns host with www' do
        expect(link_without_protocol.host).to eq("www.onet.pl")
      end
    end

    context 'domain with link' do
      it 'returns link to domain without www' do
        expect(link_with_protocol.link_to_host)
        .to eq("<a href=\"http://onet.pl\">onet.pl</a>")
      end
      it 'returns link to domain with www' do
        expect(link_without_protocol.link_to_host)
        .to eq("<a href=\"http://www.onet.pl\">www.onet.pl</a>")
      end
    end

    context 'returns host with scheme' do
      it 'returns host with scheme if host exist' do
        expect(link_with_protocol.host_with_scheme).to eq("http://onet.pl")
      end
      it 'returns host with if host doesnt exist' do
        expect(link_without_protocol.host_with_scheme).to eq("http://www.onet.pl")
      end
    end
  end

  describe "tag show helper" do
    let(:link) { FactoryGirl.create(:link, tag_list: 'first second') }
    it 'show tag(s) of link' do
      expect(link.decorate.show_tags).to eq('<span><a href="/links?tagged_with=second">#second ' \
        '</a><a href="/links?tagged_with=first">#first </a></span>')
    end
  end

  describe "video check" do
    let(:link) { FactoryGirl.build(:link).decorate }

    context 'youtube' do
      it 'if normal yt video output should not be nil' do
        link.url = "https://www.youtube.com/watch?v=H2mC77f7LQg"
        expect(link.yt_exist?).not_to be nil
      end
      it 'if embed yt video output should not be nil' do
        link.url = "https://www.youtube.com/embed/dFVxGRekRSg"
        expect(link.yt_exist?).not_to be nil
      end
    end

    context 'vimeo' do
      it 'if video ylt output should not be nil' do
        link.url = "http://vimeo.com/100981004"
        expect(link.vimeo_exist?).not_to be nil
      end
    end

    context 'video_play' do
      it 'if no video output should be nil' do
        link.url = "http://www.onet.pl"
        expect(link.video_play).to be nil
      end
    end
  end

  describe "screenshot" do
    before(:all) do
      FileUtils.rm_rf(Dir.glob("tmp/faketests/screenshots/*"))
    end

    let(:link_http) { FactoryGirl.create(:link).decorate }
    let(:link_ftp) { FactoryGirl.create(:link).decorate }

    it "returns screenshot href for http link" do
      link_http.url = "http://www.onet.pl"
      ScreenshotMaker.call(link_http)
      expect(link_http.show_screenshot).not_to be nil
    end

    it "not returns screenshot href for not http link" do
      link_ftp.url = "ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt"
      ScreenshotMaker.call(link_ftp)
      expect(link_ftp.show_screenshot).to be nil
    end
  end
end
