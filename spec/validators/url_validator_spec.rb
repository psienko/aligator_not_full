require 'spec_helper'

class Validatable
  include ActiveModel::Validations
  attr_accessor :url
  validates :url, url: true
end

describe UrlValidator do

  subject { Validatable.new }
  
  context 'valid url' do
    it 'url with http' do
      subject.url = 'http://www.onet.pl'
      expect(subject).to be_valid
    end

    it 'starts with www' do
      subject.url = 'www.onet.pl'
      expect(subject).to be_valid
    end

    it 'starts with host' do
      subject.url = 'onet.pl'
      expect(subject).to be_valid
    end

    it 'starts with ftp://' do
      subject.url = 'ftp://ftp.funet.fi/pub/standards/RFC/rfc959.txt'
      expect(subject).to be_valid
    end
  end

  context 'invalid url' do
    it 'url with http' do
      subject.url = 'http://www'
      expect(subject).to be_invalid
    end

    it 'only www' do
      subject.url = 'www.'
      expect(subject).to be_invalid
    end

    it 'url with invalid domain and without http' do
      subject.url = 'www.onet.p'
      expect(subject).to be_invalid
    end

    it 'url with http and invalid domain' do
      subject.url = 'http://www.onet.p'
      expect(subject).to be_invalid
    end
  end
end
