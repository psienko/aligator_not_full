FactoryGirl.define do

  factory :content do
    content "MyText"
    content_update_time "2010-11-11 11:11:11"
  end

  factory :link do
    url "http://onet.pl"
    tag_list  "first second"
    title "News can be here"
    link_data "http://onet.pl 'News can be here' #tag1 #tag2"
    added_from "web"
    content

    trait :with_tags do
      tag_list "first second"
    end
    user
  end

  factory :comment do
    content "MyString"
    link_id 1
  end

  factory :user do
    sequence(:email) { |n| "foo#{n}@example.com" }
    password "foobar123"
  end
end
