FactoryGirl.define do
  factory :user_subscription, class: "Subscription" do
    association :subscribeable, factory: :user
    user_id nil
  end
end
