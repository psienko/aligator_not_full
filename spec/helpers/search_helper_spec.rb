require "spec_helper"
describe SearchHelper do
  describe "#sanitize_query" do
    it "removes quotes of query" do
      expect(sanitize_query("Remove ''' quote")).to eq("Remove  quote")
    end
    it "removes double-quotes of query" do
      expect(sanitize_query('Remove"""')).to eq("Remove")
    end
  end
end
