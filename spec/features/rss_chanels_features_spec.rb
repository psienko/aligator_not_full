require 'spec_helper'

feature "subscribe RSS chanel for whole side" do
  before do
    visit root_path
  end
  scenario "check rss content" do
    link_first  = FactoryGirl.create :link, url: "http://www.onet.pl",
      tag_list: "first third second", title: "News can be find here"
    link_second = FactoryGirl.create :link, url: "http://www.o2.pl",
      tag_list: "a second c", title: "Portal"
    visit '/links.rss'
    page.has_content?('first')
    page.has_content?('http://www.onet.pl')
    page.has_content?('News can be find here')
    page.has_content?('Portal')
  end
end

feature "subscribe RSS chanel for single tag" do
  before do
    visit root_path
  end

  scenario "check rss content for single tag" do
    link_first  = FactoryGirl.create :link, url: "http://www.onet.pl",
      tag_list: "first third second", title: "News can be find here"
    link_second = FactoryGirl.create :link,  url: "http://www.interia.pl",
      tag_list: "aaa bbb ruby", title: "Portal"
    link_third = FactoryGirl.create :link, url: "http://www.portal.pl",
      tag_list: "a second ruby", title: "Inny portal"
    visit '/links.rss?tagged_with=ruby'
    page.has_content?('Portal')
    page.has_content?('Inny portal')
    page.has_content?('second')
    page.has_content?('bbb')
    page.has_content?('http://www.portal.pl')
  end
end

