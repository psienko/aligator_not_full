require 'spec_helper'

describe "Received Mail" do
  context "for logged user" do
    let(:user) { FactoryGirl.create :user, email: 'foobar@gmail.com' }
    let(:mail) { Mail.new(from: user.email,
                          to: 'aligator.pgs@gmail.com',
                          subject: "http://www.zalogowany.pl zalogowany",
                          body: "Tresc") }

    subject :link do
      EmailLinkCreator.call(mail)
    end

    it "Adds title of link from email data " do
      expect(link.title).to eq("zalogowany")
    end

    it 'Adds url of link from emaila data' do
      expect(link.url).to eq("http://www.zalogowany.pl")
    end

    it 'Adds link as a registred user' do
      expect(link.user).to eq(user)
    end
  end

  context "for guest user" do
    let!(:mail) { Mail.new(from: 'guest@outlook.com',
                           to: 'aligator.pgs@gmail.com',
                           subject: "http://www.guest.pl niezalogowany",
                           body: "Tresc")}

    subject :link do
      EmailLinkCreator.call(mail)
    end

    it "Adds title of link from email data " do
      expect(link.title).to eq("niezalogowany")
    end

    it 'Adds url of link from email data' do
      expect(link.url).to eq("http://www.guest.pl")
    end

    it 'Adds link as a guest' do
      expect(link.user_id).to be_nil
    end

  end
end
