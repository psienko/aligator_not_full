require 'spec_helper'

feature "show activity stream for logged in users" do
  before do
    @link = FactoryGirl.create :link
    ActivitiesCreator.create_link_activity(@link)
  end
  scenario "logged in user can see acivity stream" do
    log_in_with(@link.user.email, @link.user.password)
    visit root_path
    expect(page).to have_xpath('.//div[@class="activity"]')
  end
  scenario "not logged in user can't see acivity stream" do
    visit root_path
    expect(page).to have_no_xpath('.//div[@class="activity"]')
  end

  scenario "user can see in activity stream the new link added by registered user" do
    log_in_with(@link.user.email, @link.user.password)
    visit root_path
    expect(page).to have_content("#{@link.user.email} added '#{@link.title}'")
  end

  scenario "user can see in activity stream the new link added by Guest" do
    log_in_with(@link.user.email, @link.user.password)
    link = FactoryGirl.create :link, user: nil
    ActivitiesCreator.create_link_activity(link)
    visit root_path
    expect(page).to have_content("Guest added '#{link.title}'")
  end
end
