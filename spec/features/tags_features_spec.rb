require 'spec_helper'
feature "show page when click buton New Tag" do
  before do
    link = FactoryGirl.create :link
    visit "/links/#{link.id}/"
  end

  scenario "user can see informations about link" do
    click_button 'New Tag'
    expect(page).to have_content("http://onet.pl")
    expect(page).to have_content("#first")
    expect(page).to have_content("#second")
  end
end

feature "The add tag process" do
  before do
    link = FactoryGirl.create :link
    visit "/links/#{link.id}/tags/new"
  end

  scenario "fill in form and add tag" do
    fill_in "name", with: "tag3"
    click_button 'Create Tag'
    expect(page).to have_content("Tag was successfully added.")
  end

  scenario "does not fill in form and does not add tag" do
    click_button 'Create Tag'
    expect(page).to have_content("Tag was not successfully added.")
  end
end

feature 'remove tag from tags list of each link' do
  before do
    visit root_path
  end

  scenario 'should have a tag list' do
    test = FactoryGirl.create :link,
                              url: 'http://www.wp.pl',
                              tag_list: 'krystyna onet wp',
                              title: 'Wirtualna Polska'
    visit link_tags_path(test)
    page.has_content?('wp Delete')
    within("#tags") do
      find(:xpath, './/ol/li[contains(text(), "wp")]').click_link('Delete')
    end
    expect(page).to_not have_content('wp Delete')
  end
end
