require 'spec_helper'

feature "the create link process" do
  before do
    visit root_path
  end
  scenario "check selector" do
    expect(page).to have_selector('h1', text: 'Aligator')
  end
  scenario "Add new link in one input" do
    link = FactoryGirl.build :link
    user = FactoryGirl.build :user
    link.link_data = "http://blog.codeclimate.com/blog/2012/10/17/7-ways-to-decompose-fat-activerecord-models/
                      'services' #ruby"
    link.parse_and_save(link.link_data,user)
    visit "/links/#{link.id}"
    expect(page).to have_title 'Aligator'
    expect(page).to have_content("services")
    expect(page).to have_content("blog.codeclimate.com")
    expect(page).to have_content("http://blog.codeclimate.com/blog/2012/10/17/7-ways-to-decompose-fat-activerecord-models/")
  end
end

feature "show page when added link" do
  scenario "user can see youtube video " do
    link  = FactoryGirl.create :link,
      url: "https://www.youtube.com/watch?v=PEfJfJjAZFE", title: "yt"
    visit "/links/#{link.id}"

    expect(page).to have_title('Aligator -> yt')
    expect(page).to have_css('div.video_yt')
    expect(page).to have_no_css('div.video_vimeo')
  end

  scenario "user can't see youtube video" do
    link  = FactoryGirl.create :link,
      url: "http://www.google.com", title: "google"
    visit "/links/#{link.id}"
    expect(page).to have_title('Aligator -> google')
    expect(page).to have_no_css('div.video_yt')
    expect(page).to have_no_css('div.video_vimeo')
  end

  scenario "user can see vimeo video " do
    link  = FactoryGirl.create :link,
      url: "http://vimeo.com/channels/staffpicks/94449276", title: "vimeo"
    visit "/links/#{link.id}"

    expect(page).to have_title('Aligator -> vimeo')
    expect(page).to have_css('div.video_vimeo')
    expect(page).to have_no_css('div.video_yt')
  end
end

feature "show screenshot when added link" do
  scenario "user can see screenshot of page" do
    VCR.use_cassette("github_webshot") do
      link  = FactoryGirl.create :link,
        url: "https://github.com/vitalie/webshot", title: "webshot"
      visit "/links/#{link.id}"
      page.has_xpath?("/html/body/div/a/img[@alt='webshot']")
    end
  end
end

feature "show content of page when added link" do
  scenario "user can see content of page" do
    link = FactoryGirl.create(:link, content: (FactoryGirl.create :content))
    visit "/links/#{link.id}"
    expect(page).to have_xpath('.//div[@class="content_page_box"]')
  end
end

feature "display links added by user" do
  scenario "user can see links added by one user" do
    user = FactoryGirl.create(:user)
    other_user = FactoryGirl.create(:user, email: "funny@email.com")
    link = FactoryGirl.create(:link, user: user, title: "Funny title")
    link_other_user = FactoryGirl.create(:link, user: other_user, title: "Invisible")
    visit "/?user=#{user.email}"
    expect(page).to have_content(link.title)
    expect(page).to have_no_content(link_other_user.title)
  end
end

feature "display links filtered by tag" do
  scenario "user can see links filtered by tag" do
    user = FactoryGirl.create(:user)
    link = FactoryGirl.create(:link, user: user, title: "Funny title", tag_list: "funny_tag")
    visit "/?tag=#{link.tag_list[0]}"
    expect(page).to have_content(link.tag_list[0])
  end
end
