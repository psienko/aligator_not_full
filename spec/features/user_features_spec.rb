require 'spec_helper'

feature "signin in" do
  before(:all) do
    @user = User.create(email: "user@example.com", password: "foobar123",
                        password_confirmation: "foobar123")
  end
  scenario "with invalid information" do
    visit user_session_path
    click_button "Sign in"
    expect(page).to have_css("p.alert.alert-danger")
  end

  scenario "with valid information" do
    visit user_session_path
    fill_in "Email",    with: @user.email
    fill_in "Password", with: @user.password
    click_button "Sign in"
    expect(page).to have_css("p.alert.alert-success")
    expect(page).to have_content("Signed in successfully")
  end
end

feature "sigin up" do
  scenario "successfull user create by form" do
    visit new_user_registration_path
    fill_in "Email",    with: "example@example.com"
    fill_in "Password", with: "foobar1234"
    fill_in "Password confirmation", with: "foobar1234"
    expect { click_button "Sign up" }.to change(User, :count).by(1)
    expect(User.where("email = 'example@example.com'").present?).to be true
  end

  feature "add link" do
    before(:each) do
      @link = FactoryGirl.build :link
      @user = FactoryGirl.build :user, email: "user@example.com", password: "foobar123",
        password_confirmation: "foobar123"
    end
    scenario "successfull add link with user name" do
      visit user_session_path
      fill_in "Email",    with: @user.email
      fill_in "Password", with: @user.password
      click_button "Sign in"
      @link.link_data = "http://www.onet.pl 'services' #ruby"
      @link.parse_and_save(@link.link_data,@user)
      expect(@link.user_email).to eq("user@example.com")
    end
  end
end
