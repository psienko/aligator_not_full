require 'spec_helper'
feature "fulltext search" do

  before do
    visit search_links_path
  end

  let!(:link1) { FactoryGirl.create :link, title: "FINDME", tag_list: "test" }

  scenario "guest or user get alert message after submit not properly query" do
    page.find_field(:search_input).set "FIM"
    click_on("Search")
    expect(page).to have_content "Too short"
  end
end
