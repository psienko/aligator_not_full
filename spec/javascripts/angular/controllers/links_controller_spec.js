//= require spec_helper
describe('LinksController', function() { 
  var scope;

  beforeEach(inject(function($controller, $rootScope, $compile){
    scope = $rootScope.$new();
    $controller('LinksController', {$scope:scope});
  }));

  describe('watch model', function(){

    it('should set autocomplete on true when sign # is detected', function(){
      expect(scope.autocomplete).toBe(false);
      scope.model = "#";
      scope.$digest();
      scope.model = scope.model + "suggestTag"
      scope.$digest();
      expect(scope.autocomplete).toBe(true);
    });
  });

});


