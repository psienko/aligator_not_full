//= require application
//= require angular-mocks
//= require sinon
beforeEach(module('Aligator'));
beforeEach(inject(function(_$httpBackend_, _$compile_, $rootScope, $controller, $location, $injector, $timeout) {
  var _this = this;
  this.scope = $rootScope.$new();
  this.http = _$httpBackend_;
  this.compile = _$compile_;
  this.location = $location;
  this.controller = $controller;
  this.injector = $injector;
  this.timeout = $timeout;
  this.model = function(name) {
    return _this.injector.get(name);
  };
  this.eventLoop = {
    flush: function() {
      return _this.scope.$digest();
    }
  };
  return this.sandbox = sinon.sandbox.create();
}));

afterEach(function() {
  this.http.resetExpectations();
  return this.http.verifyNoOutstandingExpectation();
});
