class SubscriptionsMailer < ActionMailer::Base
  def send_user_subscription(users, link)
    @link = link
    mail(to: users, subject: "Subscription")
  end
end
