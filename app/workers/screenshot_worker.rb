class ScreenshotWorker
  include Sidekiq::Worker
  sidekiq_options queue: :screenshot

  def perform(link_id)
    link = Link.find(link_id)
    logger.info "making screenshot nr #{link.id}   - begin time  #{Time.now.to_formatted_s(:db)}"
    ScreenshotMaker.call(link)
    logger.info "finished screenshot nr #{link.id} - finish time #{Time.now.to_formatted_s(:db)}"
  end
end
