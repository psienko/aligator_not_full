class FetchContentPageWorker
  include Sidekiq::Worker
  sidekiq_options queue: :content_page

  def perform(link_id)
    link = Link.find(link_id)
    logger.info "making content page for nr #{link.id}   -
                 begin time  #{Time.now.to_formatted_s(:db)}"
    FetchContentPageMaker.call(link)
    logger.info "finished making content page for nr #{link.id} -
                 finish time #{Time.now.to_formatted_s(:db)}"
  end
end
