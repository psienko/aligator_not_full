class GithubReadmeWorker
  include Sidekiq::Worker
  sidekiq_options queue: :github_readme
  def perform(link_id)
    link = Link.find(link_id)
    logger.info "making github readme for nr #{link.id}   -
                 begin time   #{Time.now.to_formatted_s(:db)}"
    GithubReadmeMaker.call(link)
    logger.info "finished making github readme for nr #{link.id} -
                 finish time  #{Time.now.to_formatted_s(:db)}"
  end
end
