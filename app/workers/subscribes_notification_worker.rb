class SubscribesNotificationWorker
  include Sidekiq::Worker
  sidekiq_options queue: :subscribes_notification

  def perform(link_id)
    logger.info "sending notifications - begin time
                 #{Time.now.to_formatted_s(:db)}"
    link = Link.find(link_id)
    SubscriptionsSender.send_user_subscryption(link)
    logger.info "finished sending notifications -
                 finish time #{Time.now.to_formatted_s(:db)}"
  end
end
