class FetchContentPageRefresherWorker
  include Sidekiq::Worker
  sidekiq_options queue: :screenshot

  def perform(link_id)
    link = Link.find(link_id)
    logger.info "Fetching content for link nr #{link.id} -
                 begin time   #{Time.now.to_formatted_s(:db)}"
    LinkContentRefresher.call link
    logger.info "finished fetching for link nr #{link.id} -
                 finish time  #{Time.now.to_formatted_s(:db)}"
  end
end
