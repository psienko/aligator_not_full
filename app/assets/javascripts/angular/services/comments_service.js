app.factory('Comment', ['railsResourceFactory', function (railsResourceFactory) {
  return railsResourceFactory({url: window.Base + '/links/{{linkId}}/comments/{{id}}', name: 'comment'});
}]);
