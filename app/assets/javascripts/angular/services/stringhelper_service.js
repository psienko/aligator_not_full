app.service('StringHelper',function() {
  return{
    split: function(val){
      return val.split( / \s*/ );
    },
    extractLast: function(term){
      return this.split( term ).pop().slice(1);
    }
  }
});
