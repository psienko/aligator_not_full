app.factory('Tag', ['railsResourceFactory', '$http', function (railsResourceFactory, $http) {
  var resource = railsResourceFactory({url: "/links/{{linkId}}/tags", name: 'tag'});
  resource.suggest_tags = function(query){
    var self = this;
    return this.$get(window.Base + '/links/search/suggest_tags?query='+query);
  };
  return resource;
}]);
