app.controller('CommentController', ['$scope', 'Comment', function($scope, Comment) {

  var id = window.Id;

  Comment.query({}, {linkId: id}).then( function(results){
    $scope.comments = results;
  });

  $scope.createComment = function() {
    new Comment({content: $scope.comment.content, linkId: id}).create().then(
      function (results) {
      $scope.comments.unshift(results);
      $scope.commentUpdated = true;
      $scope.errors = null;
      $scope.comment.content = "";
    },
    function (error) {
      $scope.commentUpdated = false;
      $scope.errors = error.data;
    })
  };
}]);
