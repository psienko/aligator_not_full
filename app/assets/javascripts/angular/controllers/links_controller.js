app.controller('LinksController', ['$scope', 'Link', 'Tag', function($scope, Link, Tag) {

  $scope.model=null;
  $scope.tags = [];
  $scope.autocomplete = false;

  $scope.fetchLinks = function(){
    Link.query().then( function(results){
      $scope.links = results;
    });
    $scope.tag = null;
    $scope.linkUpdated = false;
  };
  $scope.fetchLinks();
  $scope.fetchLinksForTag = function(tag){
    $scope.links = Link.query({tagged_with: tag});
    $scope.tag = tag;
    $scope.linkUpdated = false;
  };

  $scope.fetchTags = function(query){
    Tag.suggest_tags(query).then( function(results){
      $scope.tags = results;
    });
  };

  $scope.createLink = function() {
    new Link({link_data: $scope.model}).create().then(
      function (results) {
      if($scope.tag){
        $scope.fetchLinks();
      }
      else{
        $scope.links.unshift(results)
      }
      $scope.linkUpdated = true;
      $scope.errors = null;
      $scope.model = "";
    }, function (error) {
      $scope.linkUpdated = false;
      $scope.errors = error.data;
    }
    )
  };

  $scope.$watch('model', function(value) {
    if (value) {
      if (value[value.length-1] == '#')
        $scope.autocomplete = true;
      if(value[value.length-1] == ' ')
        $scope.autocomplete = false;
    }
  }, true);
}]);
