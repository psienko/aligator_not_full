app.directive('autocomplete',['StringHelper', function(StringHelper) {
  return function (scope, element, attrs) {
    // http://jqueryui.com/autocomplete/#multiple
    $(element).autocomplete({
      source: function(request, response){
        if(scope.autocomplete == true){
          var query = StringHelper.extractLast(request.term);
          if(query.length > 2){
            scope.fetchTags(query);
            response($.ui.autocomplete.filter(scope.tags, query));        
          }
        }
      },
      focus:function (event, ui) {
        return false;
      },
      select:function (event, ui) {
        var terms = StringHelper.split(this.value);
        terms.pop();
        terms.push("#"+ui.item.value );
        terms.push("");
        this.value = terms.join( " " );
        scope.autocomplete = false;
        return false;
      }
    })
  };
}]);
