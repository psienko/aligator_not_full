class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ /^((https?:\/\/)|(ftp:\/\/))?([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/
      record.errors[attribute] << "Invalid URL"
    end
  end
end
