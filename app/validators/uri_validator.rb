require 'net/http'
class UriValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    uri = Addressable::URI.parse(value)
    unless uri.host.nil?
      raise Addressable::URI::InvalidURIError unless(/(.+\..+)/ === uri.host)
    else
      raise Addressable::URI::InvalidURIError unless(/(.+\..+)/ === uri.path)
    end
  rescue Addressable::URI::InvalidURIError
    record.errors[attribute] << "Invalid URL"
  end
end
