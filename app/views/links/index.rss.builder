xml.instruct! :xml, :version => "1.0"
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Links"
    xml.description "A lot of cool links"
    for link in @links
      xml.item do
        xml.title link.title
        xml.description link.url + "<br>"+"tags :#"+link.tag_list.join(" #")
        xml.pubDate link.created_at.to_s(:rfc822)
      end
    end
  end
end
