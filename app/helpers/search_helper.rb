module SearchHelper
  def sanitize_query(query)
     query.gsub("\"", '').gsub("'", "")
  end
end
