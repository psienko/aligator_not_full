class LinkSerializer < ActiveModel::Serializer
  attributes :id, :tag_list, :title, :url, :url_with_scheme, :host,
    :host_with_scheme, :created_at_status, :added_by, :comments, :number_of_likes, :user_id

  def url
    object.url
  end

  def url_with_scheme
    decorated.url_with_scheme
  end

  def host
    decorated.host
  end

  def host_with_scheme
    decorated.host_with_scheme
  end

  def link_to_host
    decorated.link_to_host
  end

  def created_at_status
    decorated.created_at_status
  end

  def decorated
    @decorated ||= LinkDecorator.new(object)
  end
end
