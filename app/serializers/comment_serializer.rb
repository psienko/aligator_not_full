class CommentSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :link_id, :content, :created, :owner_email


  def created
    decorated.created_at_status
  end

  def decorated
    @decorated ||= LinkDecorator.new(object)
  end
end
