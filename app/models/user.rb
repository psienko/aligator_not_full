class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  has_many :links
  has_many :comments
  has_many :subscriptions, as: :subscribeable
  has_many :subscribers, class_name: "Subscription"
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable
  acts_as_voter

  def can_subscribe?(subscribeable_id, subscribeable_type)
    !subscribers.exists?(subscribeable_id: subscribeable_id,
      subscribeable_type: subscribeable_type)
  end
end
