class Comment < ActiveRecord::Base
  belongs_to :link, touch: true
  belongs_to :user
  validates :link_id, :user_id, presence: true
  validates :content, length: { in: 4..250 }
  default_scope { order('created_at DESC') }

  def owner_email
    user.email
  end
end
