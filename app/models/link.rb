require "addressable/uri"

class Link < ActiveRecord::Base
  include Tire::Model::Search
  include Tire::Model::Callbacks
  include PublicActivity::Model
  has_one :content
  has_many :comments, dependent: :destroy

  after_touch() { tire.update_index }

  belongs_to :user

  validates :url, url: true
  validates :title, presence: true
  acts_as_taggable_on :tags
  acts_as_votable
  default_scope { order('created_at DESC') }
  delegate :email, to: :user, prefix: true, allow_nil: true

  tire.settings analysis: {
    analyzer: {
      full_title: {
        filter: %w(standard lowercase asciifolding),
        type: 'custom',
        tokenizer: 'standard'
      },
      partial_title: {
        filter: %w(standard lowercase asciifolding title_ngram),
        type: 'custom',
        tokenizer: 'standard'
      }
    },
    filter: {
      title_ngram: {
        side: 'front',
        max_gram: 20,
        min_gram: 2,
        type: 'edgeNGram',
        token_chars: %w(letter digit whitespace)
      }
    }
  } do
    tire.mapping do
      indexes :title,
              type: 'string', boost: 10,
              search_analyzer: 'full_title',
              index_analyzer: 'partial_title'
      indexes :comments, type: 'object'
      indexes :content, type: 'object'
      indexes :tag_list, as: 'tag_list.join(", ")'
    end
  end

  def to_indexed_json
    to_json(include: { comments: { only: [:content] },
                       content:  { only: [:github_readme] }
                      },
            methods: :tag_list)
  end

  def search(query)
    query do
      string term, default_field: 'title', default_operator: 'AND'
      match :title, term, type: 'phrase_prefix', max_expansions: 10
    end
  end
  
  def parse(data, user)
    parsed_data = {}
    parsed_data[:url] = data.split.select do |url|
      url.match(/^((http?:\/\/)|(ftp:\/\/))?([a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{0,10}(([0-9]{1,5})?\/.*)?$)/)
    end

    if URI.extract(data).empty?
      parsed_data[:url] = parsed_data[:url].join(' ').prepend('http://')
      data.slice!(parsed_data[:url].gsub('http://', ''))
    else
      parsed_data[:url] = URI.extract(data).join(' ')
      data.slice!(parsed_data[:url])
    end
    data.slice!(parsed_data[:url])
    parsed_data[:tag_list]  = data.scan(/(\#\S+)/).join(' ').gsub(/\#/, '')
    parsed_data[:title] = data.gsub(/(\#\S+)/, '').strip
    parsed_data[:user]  = user
    return parsed_data
  end

  def save_link(link_data = {})
    return false if link_data.blank?
    assign_attributes(link_data)
    save
  end
  
  def parse_and_save(data, user = nil)
    parsed_data = parse(data, user)
    save_link(parsed_data)
  end

  def screenshot_exist?
    File.exist? screenshot_path
  end

  def screenshot_root
    "#{ENV['SCREENSHOT_ROOT']}link_#{self.id}.png"
  end

  def screenshot_path
    "#{ENV['SCREENSHOT_PATH']}link_#{self.id}.png"
  end

  def http?
    @http ||= url.starts_with?('http')
  end

  def scheme_with_http?
    @scheme ||= url_with_scheme.starts_with?('http')
  end

  def url_with_scheme
    @url ||=
      if url.starts_with?('http://', 'https://', 'ftp://', 'mailto', 'ldap')
        url
      else
        "http://#{url}"
      end
  end

  def video_embed?
    /be.com\/embed\// =~ url
  end

  def ftp?
    url.starts_with?('ftp')
  end

  def content_exist?
    self.content.content.present?
  end

  def github_exist?
    true if %r{.*github.com\/.*} =~ url
  end

  def added_by
    user_email || "Guest"
  end

  def number_of_likes
    get_upvotes.size
  end


  def set_added_from_web
    self.added_from = "web"
  end

  def set_added_from_email
    self.added_from = "email"
  end

  def last_modified
    uri = URI(url)
    res = Net::HTTP.get_response(uri)
    res.get_fields("Last-Modified")
  end
end
