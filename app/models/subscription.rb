class Subscription < ActiveRecord::Base
  belongs_to :subscribeable, class_name: "User", polymorphic: true
  belongs_to :user, class_name: "User"

  def assign_user_to_subscription(user_id)
    return false if user_id == self.subscribeable.id
    return false if subscription_exist?(user_id)
    self.user_id = user_id
    self.save
  end

  def subscription_exist?(user_id)
    Subscription.where(user_id: user_id, subscribeable_id: subscribeable_id,
      subscribeable_type: subscribeable_type).present?
  end

  def self.get_user_subscription_recipients(user_id)
    user_ids = Subscription.where(subscribeable_id: user_id).pluck(:user_id)
    User.where(id: user_ids).pluck(:email)
  end
end
