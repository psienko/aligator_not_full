class ActivitiesCreator
  def self.create_link_activity(link)
    link.create_activity :create, owner: link.user
  end

  # here will be added create other activities
end
