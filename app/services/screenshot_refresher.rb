class ScreenshotRefresher
  attr_accessor :link

  def initialize(link)
    @link = link
  end

  def self.call(link)
    new(link).call
  end

  def call
    unless link.last_modified.present?
      Rails.logger.info "Screenshot nr #{@link.id} is generated. There is no Last-Modified response"
      return ScreenshotMaker.call(link)
    end
    screenshot_update_at = link.screenshot_update_time
    link_last_modified_at = Time.parse(link.last_modified.to_s).utc
    if screenshot_update_at >= link_last_modified_at
      Rails.logger.info "Screenshot nr #{link.id} update day is newer or equal Last-Modified"
    else
      Rails.logger.info "Screenshot nr #{link.id} update day is older Last-Modified.
                         New screenshot generated"
      return  ScreenshotMaker.call(link)
    end
  end
end
