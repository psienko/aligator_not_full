require 'open-uri'

class FetchContentPageMaker
  attr_accessor :link

  def initialize(link)
    @link = link
  end

  def self.call(link)
    new(link).call
  end

  def call
    @page = Nokogiri::HTML(open(link.url)).at('body').to_s
    @page.slice!("<body>")
    @page.slice!("</body>")
    if @link.content.present?
      @link.content.content_update_time = Time.now.utc
      @link.content.content = @page
      @link.content.save
    else
      link.create_content(content: @page, content_update_time: Time.now.utc)
    end
  end
end
