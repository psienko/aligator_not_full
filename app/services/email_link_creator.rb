class EmailLinkCreator
  def self.call(message)
    user = User.where(email: message.from).take
    link = Link.new
    link.set_added_from_email
    if link.parse_and_save(message.subject, user)
      if link.github_exist?
        GithubReadmeWorker.perform_async(link.id)
      else
        FetchContentPageWorker.perform_async(link.id)
      end
      SubscribesNotificationWorker.perform_async(link.id) if user.present?
      ScreenshotWorker.perform_async(link.id)
      ActivitiesCreator.create_link_activity(link)
      link
    end
  end
end
