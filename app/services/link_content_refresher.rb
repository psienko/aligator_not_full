class LinkContentRefresher
  attr_accessor :link

  def initialize(link)
    @link = link
  end

  def self.call(link)
    new(link).call
  end

  def call
    unless link.last_modified.present?
      Rails.logger.info "Fetching content page for link nr #{link.id}.
                         There is no Last-Modified response"
      refresh_content && return
    end
    refresh_content_for_link_with_last_modified
  end

  private

  def refresh_content
    link.github_exist? ? GithubReadmeMaker.call(link) : FetchContentPageMaker.call(link)
  end

  private

  def refresh_content_for_link_with_last_modified
    link_content_update_at = link.content.content_update_time
    link_last_modified_at = Time.parse(link.last_modified.to_s).utc

    if link_content_update_at <= link_last_modified_at
      Rails.logger.info "Content page for link nr #{link.id} is older than Last-Modified.
                         Fetching content page for link nr #{link.id}"
      refresh_content
    else
      Rails.logger.info "Content page for link nr #{link.id} is newer or equal Last-Modified"
    end
  end
end
