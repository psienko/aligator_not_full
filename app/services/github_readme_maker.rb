class GithubReadmeMaker
  attr_accessor :link

  def initialize(link)
    @link = link
  end

  def self.call(link)
    new(link).call
  end

  def github_exist?
    %r{.*github.com\/.*} =~ (link.url) ? true : false
  end

  def call
    return unless github_exist?
    path_github = URI(link.url).path.split("/")
    path_github.shift
    @gh_readme = Octokit.readme path_github.join("/"), accept: 'application/vnd.github.html'
    if @link.content.present?
      @link.content.content_update_time = Time.now.utc
      @link.content.content = @gh_readme
      @link.content.save
    else
      @link.create_content(content: @gh_readme, content_update_time: Time.now.utc)
    end
  end
end
