class ScreenshotMaker
  attr_reader :link

  def initialize(link)
    @link = link
  end

  def self.call(link)
    new(link).call
  end

  def call
    webshot_screenshot.capture link.url_with_scheme,
                               link.screenshot_path,
                               width: 1200,
                               height: 1500,
                               quality: 85  unless capture_link?
    @link.screenshot_update_time = Time.now.utc
    @link.save
  end

  private

  def webshot_screenshot
    @webshot_screenshot ||= Webshot::Screenshot.instance
  end

  def capture_link?
    !link.scheme_with_http? || link.video_embed? || link.ftp?
  end
end
