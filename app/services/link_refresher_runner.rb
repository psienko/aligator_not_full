class LinkRefresherRunner
  def self.call
    Link.find_each do |link|
      ScreenshotRefresherWorker.perform_async(link.id)
      FetchContentPageRefresherWorker.perform_async(link.id)
    end
  end
end
