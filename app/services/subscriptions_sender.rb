class SubscriptionsSender
  def self.send_user_subscryption(link)
    recipients = Subscription.get_user_subscription_recipients(link.user_id)
    SubscriptionsMailer.send_user_subscription(recipients, link).deliver
  end
end
