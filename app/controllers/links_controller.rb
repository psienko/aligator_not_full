class LinksController < ApplicationController
  before_action :set_link, only: [:show, :edit, :update, :destroy]
  helper_method :searched_by?
  # GET /links
  # GET /links.json
  def index
    @activities = PublicActivity::Activity.find(:all, order: "created_at desc", limit: 10)
    if params[:tag]
      @links = Link.tagged_with(params[:tag]).decorate
      searched_by("tag", params[:tag])
    elsif params[:user]
      user = User.find_by(email: params[:user])
      @links = user.links.decorate
      searched_by("user", user.email)
    else
      @links = Link.all.decorate
    end

    @link = Link.new
    respond_to do |format|
      format.html
      format.json { render json: @links }
      format.rss
    end
  end
  # GET /links/1
  # GET /links/1.json
  def show
    @comment = current_user.comments.build(params[:link_id]) if signed_in?
    @comments = @link.comments
    respond_to do |format|
      format.html
      format.json { render json: @link }
    end
  end
  # GET /links/new
  # GET /links/1/edit
  def edit
  end
  # POST /links
  # POST /links.json
  def create
    @link = Link.new
    respond_to do |format|
      @link.set_added_from_web
      if @link.parse_and_save(params[:link][:link_data], current_user)
        format.html { redirect_to @link, notice: 'Link was successfully created.' }
        format.json { render json: @link }
        ActivitiesCreator.create_link_activity(@link)
        if @link.github_exist?
          GithubReadmeWorker.perform_async(@link.id)
        else
          FetchContentPageWorker.perform_async(@link.id)
        end
        ScreenshotWorker.perform_async(@link.id)
        SubscribesNotificationWorker.perform_async(@link.id) if current_user.present?
      else
        @links = Link.all.decorate
        format.html { render action: 'index', notice: 'Link was not added' }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /links/1
  # PATCH/PUT /links/1.json
  def update
    respond_to do |format|
      if @link.update(link_params)
        format.html { redirect_to @link, notice: 'Link was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /links/1
  # DELETE /links/1.json
  def destroy
    @link.destroy
    respond_to do |format|
      format.html { redirect_to links_url }
      format.json { head :no_content }
    end
  end

  def vote_up
    @link = Link.find(params[:id])
    return redirect_to @link unless current_user
    if (current_user.voted_as_when_voted_for @link) == false
      @link.liked_by current_user
    elsif (current_user.voted_as_when_voted_for @link).nil?
      @link.liked_by current_user
    else
      @link.downvote_from current_user
    end
    redirect_to @link
  end

  private

  def searched_by(by, name)
    if by == "tag"
      @searched_by = "Links tagged with ##{name}"
    else
      @searched_by = "Links added by #{name}"
    end
  end
  # Use callbacks to share common setup or constraints between actions.
  def set_link
    @link = Link.find(params[:id]).decorate
  end

  # Never trust parameters from the scary internet,
  # only allow the white list through.
  def link_params
    params.require(:link).permit(:url, :title, :tag_list, :link_data,
                                 :gh_readme, :content, :link_id, :user_id)
  end
end
