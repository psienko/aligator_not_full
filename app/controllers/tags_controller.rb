class TagsController < ApplicationController
  def index
    @link = Link.find(params[:link_id])
  end

  def destroy
    @link = Link.find(params[:link_id])
    @link.tag_list.remove(params[:name])
    respond_to do |format|
      if @link.save
        format.html { redirect_to link_tags_path(@link), notice: 'Tag was successfully deleted!' }
      else
        format.html { redirect_to link_tags_path(@link) }
        format.json { render json: @link.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @link = Link.find(params[:link_id]).decorate
    @tag =  ActsAsTaggableOn::Tag.new
  end

  def create
    link = Link.find(params[:link_id])
    tag_name = params[:acts_as_taggable_on_tag].nil? ? " " : params[:acts_as_taggable_on_tag][:name]
    if tag_name.present? && link.tag_list.add(tag_name) && link.save
      redirect_to new_link_tag_path(link.id), notice: 'Tag was successfully added.'
    else
      redirect_to new_link_tag_path(link.id), alert: 'Tag was not successfully added.
        Please enter a tag name.'
    end
  end
end
