class SearchController < ApplicationController
  require "tag"
  include SearchHelper
  def search
    @links = Array.new
    if params[:q]
      if params[:q].length > 3
        @links = Link.search("*#{sanitize_query(params[:q])}*", load: true)
      else
        redirect_to search_links_path, alert: "Too short query"
      end
    end
  end

  def suggest_tags
    suggested_tags = ActsAsTaggableOn::Tag.suggest_tags(params[:query]).pluck(:name)
    render json: suggested_tags
  end
end
