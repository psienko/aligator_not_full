# class ApplicationController
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  include PublicActivity::StoreController
  protect_from_forgery with: :exception

  after_filter :set_csrf_cookie_for_ng
  after_filter :store_location
  hide_action :current_user
  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  def store_location
    # store last url - this is needed for post-login redirect to whatever the user last visited.
    return unless request.get?
    if request.path != new_user_session_path &&
      request.path !=  new_user_registration_path  &&
      request.path !=  new_user_password_path  &&
      request.path != edit_user_password_path &&
      request.path != destroy_user_session_path &&
      !request.xhr? # don't store ajax calls
      session[:previous_url] = request.fullpath  unless request.format.xml? ||
        request.format.json?
    end
  end

  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end

  def after_sign_out_path_for(resource)
    session[:previous_url] || root_path
  end

  protected

  def verified_request?
    super || form_authenticity_token == request.headers['X-XSRF-TOKEN']
  end
end
