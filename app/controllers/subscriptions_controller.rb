class SubscriptionsController < ApplicationController
  before_filter :authenticate_user!

  def create
    if params[:type] == 'user'
      user = User.find(params["user_id"])
      subscription = Subscription.new(subscribeable: user)
      if subscription.assign_user_to_subscription(current_user.id)
        flash[:notice] = "User has been subscribed!"
      end
    else
      #here code for tag subscryption
    end
    redirect_to :back
  end
end
