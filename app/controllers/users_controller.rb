class UsersController < ApplicationController

  def show
    set_user
    respond_to do |format|
      format.html
      format.json { render json: @user }
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
     params.require(:user).permit(:id, :email)
   end
end
