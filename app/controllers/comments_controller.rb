class CommentsController < ApplicationController
  def index
    link = Link.find(params[:link_id])
    @comments = link.comments
    render json: @comments
  end

  def create
    link = Link.find(params[:link_id])
    @comment = link.comments.build(comment_params)
    @comment.user = current_user
    if @comment.save
      render json: @comment
    else
      render json: @comment.errors, status: :unprocessable_entity
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :link_id, :user_id)
  end
end
