module ActsAsTaggableOn
  class TagDecorator < Draper::Decorator
    def show_tag
     "##{object} "
    end
  end
end
