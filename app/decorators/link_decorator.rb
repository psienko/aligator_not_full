class LinkDecorator < Draper::Decorator
  include Draper::LazyHelpers
  delegate_all

  def show_tags
    h.content_tag(:span) do
      object.tags.map do |tag|
        h.concat(h.link_to(tag.decorate.show_tag, h.links_path(tagged_with: tag.name)))
      end
    end
  end
  ################################################################################
  def host
    @host ||= URI(url_with_http).host
  end

  def show_link_owner
    if object.user.present?
      h.link_to object.added_by, links_path(user: object.user.email)
    else "Guest"
    end
  end

  def url_with_http
    @url ||= if object.http?
               object.url
             else
               url_with_scheme
             end
  end

  def link_to_host
    h.link_to host, host_with_scheme
  end

  def path_of_url
    URI(object.url).path
  end

  def host_with_scheme
    @host_with_scheme ||= "http://#{host}"
  end
  ################################################################################
  def created_at_status
    @created_at_status ||= h.time_ago_in_words(object.created_at)
  end

  def created_at_status_detailed
    @created_at_status_deatailed ||= object.created_at.localtime.strftime("%F at %T")
  end
  ################################################################################
  def show_screenshot
    h.link_to image_tag(screenshot_root, alt: object.title), object.decorate.host_with_scheme,
      class: "screenshot" if object.screenshot_exist?
  end
  ################################################################################
  # #################################VIDEO#########################################
  def yt_play
    yt_match = /youtu\.be\/([^\?]*)/.match(url)
    youtube_id = if yt_match
                   yt_match[1]
                 else
                   /^.*((v\/)|(embed\/)|(watch\?))\??v?=?([^\&\?]*).*/.match(url)[5]
                 end

    raw(%Q({<div class="video_yt"><iframe title="YouTube video player" width="640" height="390"
           src="https://www.youtube.com/embed/#{ youtube_id }"
           frameborder="0" allowfullscreen></iframe></div>}))
    # source : http://stackoverflow.com/questions/5909121/converting-a-regular-youtube-link-into-an-embedded-video
  end

  def vimeo_play
    vimeo_id = /(?:www\.)?vimeo.com\/([a-z]+)?\/?([a-z]+)?\/?(\d+)?/.match(url)[3]
    raw(%Q({<div class="video_vimeo"><iframe src="//player.vimeo.com/video/#{vimeo_id}" width="640"
           height="390" frameborder="0" webkitallowfullscreen mozallowfullscreen
           allowfullscreen></iframe></div>}))
  end

  def yt_exist?
    %r{.*youtu\.*be.*} =~ object.url
  end

  def vimeo_exist?
    %r{.*vimeo.com\/.*} =~ (object.url)
  end

  def video_play
    if yt_exist?
      @yt ||= yt_play
    elsif vimeo_exist?
      @vimeo ||= vimeo_play
    end
  end

  def content
    unless object.content.nil? || object.content.content.nil?
      content_tag :div, class: "content_page_box" do
        object.content.content.html_safe
      end
    end
  end
end
