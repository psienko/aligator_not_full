module PublicActivity
  class ActivityDecorator < Draper::Decorator
    def show_link_activity
      ("added #{show_title} #{object.trackable.decorate.show_tags} " \
        "#{added_from} #{h.link_to time_ago, object.trackable}").html_safe
    end

    def display_activity_owner
      object.owner ? user : "Guest"
    end

    def user
      h.link_to object.owner.email, object.owner
    end

    def added_from
      "from #{object.trackable.added_from}" if object.trackable.added_from
    end

    def time_ago
      "#{h.distance_of_time_in_words_to_now(object.trackable.created_at)} ago"
    end

    def show_title
      h.link_to "'#{object.trackable.title}'", object.trackable.url
    end
  end
end
