class CommentDecorator < Draper::Decorator
  delegate_all

  def created_at_status
    @created_at_status ||= h.time_ago_in_words(object.created_at)
  end

end
