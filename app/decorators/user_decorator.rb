class UserDecorator < Draper::Decorator
  def subscribe_user
    if h.current_user.present? && h.current_user.can_subscribe?(object.id, object.class.name) &&
      object != h.current_user
      h.button_to 'Subscribe user!', h.subscriptions_path(user_id: object.id, type: "user"),
                  method: :post, class: "button"
    end
  end
end
