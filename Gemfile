source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.2'
gem 'nokogiri'
gem 'execjs'
gem 'therubyracer'
gem 'acts-as-taggable-on', '~> 3.3.0'
# Use postgresql as the database for Active Record
gem 'pg'
gem 'draper', '~> 1.3'
gem 'addressable', '~> 2.3.6'     # URL
gem 'webshot', '~> 0.0.7'     #screenshot maker
gem 'sidekiq', '~> 3.2.1'
gem 'sidekiq-limit_fetch'
gem 'sinatra', require: false

gem 'whenever', '~> 0.9.2' , require: false

gem 'acts_as_votable', '~> 0.10.0'
gem 'devise', '~> 3.2.3'

gem 'mailman', '~> 0.7.2', require:false
gem 'maildir', '~> 2.1.0' ##
gem 'mailman-rails', '~> 0.0.3'
gem 'daemons', '~> 1.1.9'

gem 'angularjs-rails', '~> 1.2.21'
gem 'active_model_serializers', '~> 0.8.1'
gem 'angularjs-rails-resource', '~> 1.1.1'
gem 'public_activity' , '~> 1.4.2'

group :development, :test do
  gem 'spring-commands-rspec'
  gem 'rspec-rails', '~> 3.0.0'
  gem "shoulda"
  gem 'guard-rspec'
  gem "factory_girl_rails", "~> 4.0"
  gem 'rspec-collection_matchers'
  gem 'pry-byebug'
  gem 'rubocop', '~> 0.24.1', require: false

  gem 'jasmine-rails'
  gem 'sinon-rails'
  gem 'jasmine-sinon-rails'
  gem 'jasmine-jquery-rails'
  gem 'poltergeist'
  gem 'vcr', '~> 2.4'
end

group :development do
  gem 'guard'
  gem 'pry-rails'
  gem "better_errors"
  gem 'binding_of_caller'
  gem 'quiet_assets'
  gem 'capistrano', '~> 3.2.0'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-bundler', '~> 1.1'
  gem 'capistrano-rvm'
  gem 'capistrano-sidekiq'
  gem 'rvm-capistrano'
  gem 'thin', '~> 1.6.2'
  gem 'letter_opener'
end

group :test do
  gem 'capybara', '~> 2.3.0'
  gem "capybara-webkit"
  gem 'webmock', '~> 1.18.0'
  gem 'database_cleaner', '~> 1.3.0'
  gem 'phantomjs', '~> 1.9.7.1'
end

# ElasticSearch
gem 'elasticsearch-model', '~> 0.1.4'
gem 'elasticsearch-rails', '~> 0.1.4'

#Tire
gem 'tire', '~> 0.6.2'

#GitHub api gem
gem "octokit", "~> 3.0"
# Using bootstrap
gem 'bootstrap-sass', '2.3.2.0'
# Using font awsome
gem 'font-awesome-sass'
# Using simple form
gem 'simple_form', '~> 3.0.2'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Not Use CoffeeScript for .js.coffee assets and views
# gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.1.2'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
