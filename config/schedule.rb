set :output, "#{path}/log/cron.log"

every 1.day, :at => '2:30 am' do
  runner "LinkRefresherRunner.call"
end
