# config valid only for Capistrano 3.1
lock '3.2.1'
set :rails_env, 'production'
set :application, 'aligator'
set :repo_url, 'git@gitlab.com:pgs-ruby-training/aligator.git'

SSHKit.config.command_map[:rake] = "bundle exec rake"
SSHKit.config.command_map[:rails] = "bundle exec rails"

set :sidekiq_config, "~/deployment/shared/config/sidekiq.yml"
set :sidekiq_cmd, "#{fetch(:bundle_cmd, "bundle")} exec sidekiq -C config/sidekiq.yml"
# RAILS_ENV=production exec bin/mailman_daemon start"

set :bundle_jobs, 4 # This is only available for bundler 1.4+

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '~/deployment'

# set :branch, fetch(:branch, "devise")
# set :env, fetch(:env, "production")
# Default value for :scm is :git
set :scm, :git

set :deploy_via, :remote_cache

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/local_env.yml config/sidekiq.yml}
set :linked_dirs, %w{public/screenshots tmp/pids}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :rails_env, 'production'
# Default value for keep_releases is 5
set :keep_releases, 5
namespace :mailman do
  desc 'Stop Mailman'
  task :stop do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do 
          execute :bundle, :exec, 'bin/mailman_daemon stop'
        end
      end
    end
  end
  desc 'Start Mailman'
  task :start do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, 'bin/mailman_daemon start'
        end
      end
    end
  end

  desc 'Status Mailman'
  task :status do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :bundle, :exec, 'bin/mailman_daemon status'
        end
      end
    end
  end
end

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :mkdir, '-p', "#{ release_path }/tmp"
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end
