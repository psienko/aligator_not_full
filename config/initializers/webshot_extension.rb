require 'webshot'
module Webshot
  class<< self
    alias_method :old_setup, :capybara_setup!
    def capybara_setup!
      Capybara.run_server = false
      Capybara.register_driver :poltergeist do |app|
        Capybara::Poltergeist::Driver.new(app,
                                          js_errors: false,
                                          timeout: 180,
                                          phantomjs_options: ['--ignore-ssl-errors=yes']
                                          )
      end
      Capybara.current_driver = :poltergeist
    end
  end
end
