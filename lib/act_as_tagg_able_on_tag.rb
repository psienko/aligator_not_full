module ActsAsTaggableOn
  class Tag < ActiveRecord::Base
    scope :suggest_tags, ->(query){where("name LIKE :query", query: "#{query}%")}
  end
end
